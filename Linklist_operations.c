 ////  push pop value merge reverse print Alternate_A_then_B

#include<stdio.h>

typedef struct list
{
   int data;
   struct list * next;
}node;
   

node * delete(node* ,int);
node * delete(node* head,int loc)
{
	node *start=head;
	node *del;
	if(loc==0)
	{
		del=head;
		printf(" value deleted  : %d ",del->data);
		head=head->next;
		free(del);
		return head;
	}
    int count=0;
    node* prev;
	while(count<loc)
	 {
	 	prev=head;
	 	head=head->next;
	 	count++;
	 }
	del=head;
	printf(" value deleted  : %d ",del->data);
	prev->next=head->next;
	free(del);
    return start;	
}

node* insert(node * ,int ,int );
node* insert(node* head,int value, int loc)
{
	node *start=head;
	node *new=(node*)malloc(sizeof(node));
	new->data=value;
	new->next=NULL;
	if(loc==0)
	{
        new->next=head;
        return new;
	}
	
	int count=0;
	while(count<loc)
	{
	   head=head->next;
	   count++;
	}
	node *next=head->next;
	head->next=new;
	new->next=next;
	return start;
}

node* appendalternate(node *,node *);
node* appendalternate(node * a,node * b)
{
	if(b==NULL)
         return a;
 	if(a==NULL)
         return b;    
         
    node * head;
    node * start=(node *)malloc(sizeof(node));
    head =start;
    node *new;

	start->data=a->data;
	start->next=NULL;
	a=a->next;

    while(a && b) 
      {
      	printf("\n  b fill ");
		new =(node*)malloc(sizeof(node));
		new->data=b->data;
		new->next=NULL;
		start->next=new;
		start=new;
		b=b->next;       	

        printf("\n  a fill ");
    	new=(node*)malloc(sizeof(node));
    	new->data=a->data;
    	new->next=NULL;
    	start->next=new;
    	start=new;
    	a=a->next;   

       }
       
    while(a != NULL)
      {
      	printf(" \n a left ");
    	new=(node*)malloc(sizeof(node));
    	new->data=a->data;
    	new->next=NULL;
    	start->next=new;
    	start=new;
    	a=a->next;
      }

    while(b != NULL)
      {
      	printf(" \n b left ");
		new =(node*)malloc(sizeof(node));
		new->data=b->data;
		new->next=NULL;
		start->next=new;
		start=new;
		b=b->next;
      }
    return head;

}

node * push(node* , int );
node * push(node* head, int value)
{
   node * new;
   new=(node*)malloc(sizeof(node));
   new->data=value;
   new->next=NULL;
   head->next=new;
   head=new;
   return head;
}

node * reverse(node*);
node * reverse(node* head)
{
   node* prev   = NULL;
   node* current = head;
   node* next;
   while (current != NULL)
    {
        next  = current->next;  
        current->next = prev;   
        prev = current;
        current = next;
    }
   return prev;
}


void pop(node* , int );
void pop(node* head, int value)
{
   node* prev;
   prev=head;
   int flag=0;
   while(head)
    {
        if(head->data==value)
        {
            flag=1;
            break;
        }
        prev=head;
        head=head->next;
    }
    if(flag=1)
        prev->next=head->next;
    free(head);
}
  
void printlist(node *);
void printlist(node * start)
{
  while(start)
    {
       printf(" -- %d ",start->data);
       start=start->next;
    }
}

node* merge(node *,node *);
node* merge(node * a,node * b)
{
	if(b==NULL)
         return a;
 	if(a==NULL)
         return b;    
         
    node * head;
    node * start=(node *)malloc(sizeof(node));
    head =start;
    node *new;
    
    if(a->data < b->data)
    {
    	start->data=a->data;
    	start->next=NULL;
    	a=a->next;
    }
    else 
    {
		start->data=b->data;
		start->next=NULL;
		b=b->next;
    }
     
    while(a && b) 
    
       {
       	      if(a->data<b->data)
       	         {
       	         	
       	         	new=(node*)malloc(sizeof(node));
       	         	new->data=a->data;
       	         	new->next=NULL;
       	         	start->next=new;
       	         	start=new;
       	         	a=a->next;
       	         }
       	       else if(a->data > b->data)
     	         {
					new =(node*)malloc(sizeof(node));
					new->data=b->data;
					new->next=NULL;
					start->next=new;
					start=new;
					b=b->next;
       	         }
       	        else
       	        {
					new =(node*)malloc(sizeof(node));
					new->data=b->data;
					new->next=NULL;
					start->next=new;
					start=new;
					b=b->next;
					a=a->next;
					
       	        }
       }
       
    if(a != NULL)
      {
    	new=(node*)malloc(sizeof(node));
    	new->data=a->data;
    	new->next=NULL;
    	start->next=new;
    	start=new;
    	a=a->next;
      }

    if(b != NULL)
      {
		new =(node*)malloc(sizeof(node));
		new->data=b->data;
		new->next=NULL;
		start->next=new;
		start=new;
		b=b->next;
      }
    return head;

}


int main()
{
   node *a;
   a=(node*)malloc(sizeof(node));
   a->data=0;
   node *start;
   start=a;
   start=push(start,3);
   start=push(start,7);
   start=push(start,11);
   start=push(start,20);
   printf(" \n NEW LIST a : ");
   printlist(a);

   node *b;
   b=(node*)malloc(sizeof(node));
   b->data=1;
   start=b;
   start=push(start,2);
   start=push(start,9);
   start=push(start,11);
   start=push(start,33);
   printf(" \n NEW LIST b : ");
   printlist(b);
   
   node *c;
   
   c= merge(a,b);
   printf(" \n NEW LIST c : ");
   printlist(c);
   
   node * d;
   d=reverse(c);
   printf(" \n NEW LIST reverse c d : ");
   printlist(d);
   
   
   return 0;
}